//*****************************************************************************
// Global variable declarations
//*****************************************************************************

debug = false;
//username = "Player";

//*****************************************************************************
// Randomize
//*****************************************************************************

randomize();

//*****************************************************************************
// Set camera target
//*****************************************************************************

camera_target = obj_Player;
camera_shake = false;
camera_smoothness = 0.08;

//*****************************************************************************
// Menu item definitions
//*****************************************************************************

menu_items = array_create(4);
menu_items[0] = "Start game";
menu_items[1] = "Help";
menu_items[2] = "Options";
menu_items[3] = "Credits";
menu_items[4] = "Quit";

option_items = array_create(5);
option_items[0] = "Music";
option_items[1] = "Sounds";
option_items[2] = "Volume";
option_items[3] = "Fullscreen";
option_items[4] = "Controls";
option_items[5] = "Name";

control_indices = [];
controls = ds_map_create();
control_names = ds_map_create();

control_indices[0] = "left";
control_indices[1] = "right";
control_indices[2] = "up";
control_indices[3] = "down";

controls[? "left"] = ord("A");
controls[? "right"] = ord("D");
controls[? "up"] = ord("W");
controls[? "down"] = ord("S");


control_names[? "left"] = "Move Left";
control_names[? "right"] = "Move Right";
control_names[? "up"] = "Move Up";
control_names[? "down"] = "Move Down";

name_being_modified = true;

wait_for_input = false;
key_being_remapped = noone;

option_type = ds_map_create();
ds_map_add(option_type, option_items[0], "toggle");
ds_map_add(option_type, option_items[1], "toggle");
ds_map_add(option_type, option_items[2], "slider");
ds_map_add(option_type, option_items[3], "toggle");
ds_map_add(option_type, option_items[4], "");
ds_map_add(option_type, option_items[5], "input");

option_value = ds_map_create();
ds_map_add(option_value, option_items[0], true);
ds_map_add(option_value, option_items[1], true);
ds_map_add(option_value, option_items[2], 1.0);
ds_map_add(option_value, option_items[3], false);
ds_map_add(option_value, option_items[4], noone);
ds_map_add(option_value, option_items[5], "Player");

credits[0] = "[fnt_Menu][fa_middle][fa_center][c_white]2021[c_yellow]Programming: [c_white]biyectivo";
credits[1] = "[fa_middle][fa_center][c_white][c_white](Jose Alberto Bonilla Vera)";

game_title = "Clone Pinball";
scoreboard_game_id = "ClonePinball";

primary_gamepad = -1;
start_drag_drop = false;

// Particle system and emitter variables - define to avoid not set
particle_system = noone;
particle_emitter = noone;

pause_screenshot = noone;

gui_offset_x = 0;
gui_offset_y = 0;
application_surface_scaling = 1;


//*****************************************************************************
// Data structures
//*****************************************************************************
grid = noone;

//*****************************************************************************
// Shader stuff
//*****************************************************************************

#region Shaders
/*
u_brightness = shader_get_uniform(shd_BriConSat, "brightness");
u_contrast = shader_get_uniform(shd_BriConSat, "contrast");
brightness = 1;
contrast = 1;

u_brightness = shader_get_uniform(shd_BriConSat, "brightness");
u_contrast = shader_get_uniform(shd_BriConSat, "contrast");
u_saturation = shader_get_uniform(shd_BriConSat, "saturation");
brightness = 1;
contrast = 1;
saturation = 1;
u_shd_sat_strength = shader_get_uniform(shd_Saturation, "strength");
strength = 0.5;
u_contrast = shader_get_uniform(shd_Contrast, "contrast");
contrast = 1;
u_gamma = shader_get_uniform(shd_Gamma, "gamma");
gamma = 1;
u_brightness = shader_get_uniform(shd_Brightness, "brightness");
brightness = 0;
u_shd_desat_strength = shader_get_uniform(shd_Desaturation, "strength");
strength = 0;

u_duotone_color_r = shader_get_uniform(shd_DuoTone_Color, "duotone_color_r");
u_duotone_color_g = shader_get_uniform(shd_DuoTone_Color, "duotone_color_g");
u_duotone_color_b = shader_get_uniform(shd_DuoTone_Color, "duotone_color_b");
duotone_color_r = color_get_blue(global.tf_colors[?"sepia"])/255;
duotone_color_g = color_get_green(global.tf_colors[?"sepia"])/255;
duotone_color_b = color_get_red(global.tf_colors[?"sepia"])/255;

// only one handler is needed if we pass as array to a vec3
u_duotone_color = shader_get_uniform(shd_DuoTone_Color, "duotone_color");
duotone_color = [1.1, 1.0, 0.8];



u_duotone_stylized_light = shader_get_uniform(shd_DuoTone_Stylized, "duotone_light");
u_duotone_stylized_dark = shader_get_uniform(shd_DuoTone_Stylized, "duotone_dark");
duotone_stylized_light = [1.2, 1.2, 0.6];
duotone_stylized_dark = [0.1, -0.4, 0.3];

u_overlay_color = shader_get_uniform(shd_Overlay_Color, "overlay_color");
overlay_color = [0.5, 0.5, 0.5];
*/


#endregion

fnc_InitializeGameStartVariables();

//show_debug_overlay(Game.debug);
//show_debug_overlay(true);


num_strings = 1;