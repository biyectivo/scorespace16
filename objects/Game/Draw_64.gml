
fnc_BackupDrawParams();

switch (room) {
	/*case room_UI_Title:
		fnc_DrawMenu();
		break;
	case room_UI_Options:
		fnc_DrawOptions();
		break;
	case room_UI_Options_Controls:
		fnc_DrawOptionsControls();
		break;
	case room_UI_Credits:
		fnc_DrawCredits();		
		break;
	case room_UI_HowToPlay:
		fnc_DrawHowToPlay();
		break;*/
	default:		
		if (Game.paused) {
			if (Game.lost) {
				instance_deactivate_all(true);
				instance_activate_object(obj_TypeFormatted);
				fnc_DrawYouLost();
			}
			else if (Game.just_started) {
				fnc_DrawTitle();
			}
			else {				
				fnc_DrawPauseMenu();
			}
		}
		else {
			fnc_DrawHUD();
		}
		break;
}



fnc_RestoreDrawParams();
