/// @description Pause
if (room == room_Game_1) {
	if (!Game.lost) {			
		paused = !paused;		
		if (paused) {
			physics_pause_enable(true);
			screen_save_part("_tmp_screenshot.png", 0, 0, window_get_width(), window_get_height());
			pause_screenshot = sprite_add("_tmp_screenshot.png", 0, false, false, 0, 0);
			instance_deactivate_all(true);
			instance_activate_object(obj_TypeFormatted);
		}
		else {
			physics_pause_enable(false);
			instance_activate_all();
		}
	}
}