// Enable views and set up graphics
view_enabled = true;
view_visible[0] = true;
fnc_SetGraphics();

// Stop all sounds and set up sound volume
audio_stop_all();
audio_master_gain(option_value[? "Volume"]);	


if (room == room_Game_1) {
		
	physics_world_create(0.1);
	physics_world_gravity(0, 25);
			
	
	// Initialize particle system and emitters
		
	particle_system = part_system_create_layer("lyr_Instances", true);
	particle_emitter = part_emitter_create(particle_system);
		
	// Initialize player
	if (instance_exists(obj_Player)) {
		with (obj_Player) {
			event_perform(ev_other, ev_user0);
		}
	}
	
	// (Re)initialize game start variables		
	fnc_InitializeGameStartVariables();
	
	
	
	
	
	// Scribble - add fonts to included files first
	//scribble_font_add("fnt_Title");
		
}
