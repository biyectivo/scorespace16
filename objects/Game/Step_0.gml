if (ENABLE_LIVE && live_call()) return live_result;

if ( (just_started && device_mouse_check_button_pressed(0, mb_left) && instance_number(obj_Player) == 0)) {	
	just_started = false;
	paused = false;
	lost = false;	
	total_score = 0;
	clone_combo = 0;
	fnc_CreateClones(1, 1, true);
	// Play music	
	if (option_value[? "Music"]) {
		music_sound_id = audio_play_sound(snd_Music, 1, true);
	}
}

if (device_mouse_check_button_pressed(0, mb_left) && lost && paused) {
	game_restart();	
}

if (!paused && !just_started && instance_number(obj_Player) == 0 && obj_CloneTarget.alarm[1] <= 0 && obj_CloneTarget.alarm[2] <= 0) {
	if (alarm[3] == -1) {
		lost = true;
		if (audio_is_playing(music_sound_id)) {
			audio_sound_gain(music_sound_id, 0, 60);	
			audio_play_sound(snd_GameOver, 2, false);
		}
		
		fnc_UpdateScoreboard();
		alarm[3] = 60;
	}
}

if (Game.debug && device_mouse_check_button_pressed(0, mb_left)) {
	instance_create_layer(ROOM_MOUSE_X, ROOM_MOUSE_Y, "lyr_Instances", obj_Player);
}



// Hide collision layer	

if (layer_exists(layer_get_id("lyr_Tile_Collision"))) {
	layer_set_visible(layer_get_id("lyr_Tile_Collision"), debug);
}


// Room-specific code
if (!Game.paused) {
	if (room == room_Game_1) {
		
		// Register swipe/drag	
		device_mouse_dragswipe_register(0);
		
		// Global step
		if (instance_exists(obj_Player)) {
			current_step++;
		}		
	}	
}



