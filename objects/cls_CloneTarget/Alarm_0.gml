/// @description Return to normal
with (light) {
	alpha = 1;
	blend = $dddddd;
}