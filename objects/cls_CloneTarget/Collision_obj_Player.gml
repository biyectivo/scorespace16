if (!Game.paused) {
	with (light) {
		alpha = 1;
		blend = c_white;
	}
	alarm[0] = alarm_time;
}