light = new BulbLight(obj_Bulb.renderer, sLight512, 0, x, y);

with (light) {
	alpha = 1;
	blend = $dddddd;
}