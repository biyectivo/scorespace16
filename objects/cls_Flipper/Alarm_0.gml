/// @description Return to normal
with (light) {
	alpha = 0.7;
	blend = $5e3c46;
}