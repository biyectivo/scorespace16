if (!Game.paused) {
	with (light) {
		alpha = 1;
		blend = $76475d;
	}
	alarm[0] = alarm_time;
	if (Game.option_value[? "Sounds"]) {
		audio_play_sound(snd_Flipper, 2, false);	
	}
}