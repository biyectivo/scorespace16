if (!Game.paused) {
	if (Game.debug && keyboard_check(vk_space)) {
		physics_draw_debug();
	}
	else {
		if (alarm[0] > 0) {
			outline_start(2, $76475d, sprite_index, 64, 0);
		}
		draw_self();
		if (alarm[0] > 0) {
			outline_end();
		}
	}
}