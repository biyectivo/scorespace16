if (!Game.paused) {
	with (light) {
		alpha = 1;
		blend = $f7fd5e;
	}
	alarm[0] = alarm_time;
	if (Game.option_value[? "Sounds"]) {
		audio_play_sound(snd_Bump, 3, false);
	}
}