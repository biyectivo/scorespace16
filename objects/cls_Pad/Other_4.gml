light = new BulbLight(obj_Bulb.renderer, sLight512, 0, x, y);

with (light) {
	xscale = 0.5;
	yscale = 0.5;
	alpha = 0.8;
	blend = $a59345;
}