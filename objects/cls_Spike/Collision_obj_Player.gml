if (!Game.paused) {
	with (light) {
		alpha = 1;
		blend = c_red;
	}
	alarm[0] = alarm_time;
}