light = new BulbLight(obj_Bulb.renderer, sLight512, 0, x, y);

with (light) {
	xscale = 0.25;
	yscale = 0.25;
	alpha = 0.7;
	blend = $dddddd;
}