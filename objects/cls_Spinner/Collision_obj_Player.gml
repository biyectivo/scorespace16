if (!Game.paused) {
	with (light) {
		alpha = 1;
		blend = c_lime;
	}
	alarm[0] = alarm_time;
	if (Game.option_value[? "Sounds"]) {
		audio_play_sound(snd_Click, 2, false);	
	}
}