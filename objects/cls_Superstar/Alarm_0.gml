/// @description Return to normal
with (light) {
	alpha = 0.8;
	blend = $8a4276;
}