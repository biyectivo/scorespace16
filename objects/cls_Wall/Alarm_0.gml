/// @description Return to normal
with (light) {
	alpha = 0.7;
	blend = $a858ab
}