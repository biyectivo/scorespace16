if (!Game.paused) {
	with (light) {
		alpha = 1;
		blend = $cc5dff;
	}
	alarm[0] = alarm_time;
	if (Game.option_value[? "Sounds"]) {
		audio_play_sound(snd_Click, 3, false);
	}
}