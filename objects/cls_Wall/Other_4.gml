light = new BulbLight(obj_Bulb.renderer, sLight512, 0, x, y);

with (light) {
	xscale = 0.125;
	yscale = 0.125;
	alpha = 0.7;
	blend = $a858ab;
}