var _id = instance_create_layer(x, y, "lyr_Points", obj_Text);
Game.clone_combo++;
with (_id) {
	text = "[fa_center][fa_middle][scale,0.3][c_black][fnt_Title_1]CLONING [c_lime]x"+string(Game.clone_combo);
}
fnc_CreateClones(Game.clone_combo, Game.clone_combo);