if (!Game.paused) {
	if (create_clones) {
		if (Game.option_value[? "Sounds"]) {
			audio_play_sound(snd_Clone, 2, false);
		}
		Game.total_score = Game.total_score + POINTS_CLONE;
		var _id = instance_create_layer(x, y, "lyr_Points", obj_Text);
		with (_id) {
			text = "[fa_center][fa_middle][fnt_Title_2][scale,0.3][c_white]+"+string(POINTS_CLONE);
		}
		alarm[2] = 45;
		alarm[1] = 60*(cooldown + Game.clone_combo);
		create_clones = false;
	}
}