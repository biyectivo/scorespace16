if (!Game.paused) {
	fnc_BackupDrawParams();
	if (!create_clones) {
		image_alpha = 0.3;
	}
	else {
		image_alpha = 1;	
	}
	draw_self();
	fnc_RestoreDrawParams();
}