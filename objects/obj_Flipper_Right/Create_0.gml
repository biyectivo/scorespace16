event_inherited();
var fixture = physics_fixture_create();
physics_fixture_set_polygon_shape(fixture);
physics_fixture_add_point(fixture, 0, 31);
physics_fixture_add_point(fixture, 4, 28);
physics_fixture_add_point(fixture, 51, 20);
physics_fixture_add_point(fixture, 62, 26);
physics_fixture_add_point(fixture, 63, 31);
physics_fixture_add_point(fixture, 62, 37);
physics_fixture_add_point(fixture, 51, 43);
physics_fixture_add_point(fixture, 4, 35);
physics_fixture_set_density(fixture, 0);
physics_fixture_set_restitution(fixture, RESTITUTION_FLIPPER_DEFAULT);
physics_fixture_set_linear_damping(fixture, 0);
physics_fixture_set_angular_damping(fixture, 0);
physics_fixture_set_friction(fixture, 0.1);
if (!BROWSER) {
	physics_fixture_bind_ext(fixture, id, -54, -32);
}
else {
	physics_fixture_bind_ext(fixture, id, 54, 32);
}
physics_fixture_delete(fixture);