if (!Game.paused) {
	if (keyboard_check(ord("D"))) {
		phy_rotation = min(max(0, phy_rotation+5), base_rotation + 45);
	}
	else {
		phy_rotation = max(phy_rotation - 5, base_rotation);
	}
}