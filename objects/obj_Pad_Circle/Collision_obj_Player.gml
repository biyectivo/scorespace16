event_inherited();
Game.total_score = Game.total_score + POINTS_CIRCLE;
if (Game.option_value[? "Sounds"]) {
	audio_play_sound(snd_Pad, 2, false);
}
var _id = instance_create_layer(x, y, "lyr_Points", obj_Text);
with (_id) {
	text = "[fa_center][fa_middle][fnt_Title_2][scale,0.2][c_white]+"+string(POINTS_CIRCLE);
}