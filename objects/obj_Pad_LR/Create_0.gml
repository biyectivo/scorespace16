event_inherited();
var fixture = physics_fixture_create();
physics_fixture_set_polygon_shape(fixture);
physics_fixture_add_point(fixture, sprite_width, 0);
physics_fixture_add_point(fixture, sprite_width, sprite_height);
physics_fixture_add_point(fixture, 0, sprite_height);
physics_fixture_set_density(fixture, 0);
physics_fixture_set_restitution(fixture, RESTITUTION_TRIANGLE);
physics_fixture_set_linear_damping(fixture, 0.6);
physics_fixture_set_angular_damping(fixture, 0.6);
physics_fixture_set_friction(fixture, 0.2);
if (!BROWSER) {
	physics_fixture_bind_ext(fixture, id, -sprite_width/2, -sprite_height/2);
}
else {
	physics_fixture_bind_ext(fixture, id, sprite_width/2, sprite_height/2);	
}
physics_fixture_delete(fixture);