/// @description Collide
/*
var _object_name = object_get_name(other.object_index);
show_debug_message("Collided with "+_object_name+" with a speed of "+string(phy_linear_velocity_x)+","+string(phy_linear_velocity_y));
*/
if (!Game.paused) {
	if (other.object_index == obj_Spike) {	
		// Emit
		part_emitter_region(Game.particle_system, Game.particle_emitter, x-sprite_width/2, x+sprite_width/2, y-sprite_height/2, y+sprite_height/2, ps_shape_ellipse, ps_distr_gaussian);
		part_emitter_burst(Game.particle_system, Game.particle_emitter, particle_type_player_bit, 200);	
		instance_destroy();	
	}
}