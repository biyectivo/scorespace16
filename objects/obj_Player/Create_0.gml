/*
var fixture = physics_fixture_create();
physics_fixture_set_circle_shape(fixture, 5)
physics_fixture_set_density(fixture, 0.5);
physics_fixture_set_restitution(fixture, 0.1);
physics_fixture_set_friction(fixture, 0.2);
physics_fixture_bind_ext(fixture, self, 17-16, 9);
physics_fixture_delete(fixture);*/
image_index = 6;

t = 0;
num_frames_nearzero = 0;

particle_type_player_bit =	part_type_create();		
part_type_scale(particle_type_player_bit, 1, 1);
part_type_size(particle_type_player_bit, 0.05, 0.1, 0, 0);
part_type_life(particle_type_player_bit, 5, 25);			
part_type_shape(particle_type_player_bit, pt_shape_disk);
part_type_alpha2(particle_type_player_bit, 1, 0);
part_type_speed(particle_type_player_bit, 2, 5, 0, 0);
part_type_direction(particle_type_player_bit, 0, 360, 0, 0);
part_type_orientation(particle_type_player_bit, 0, 0, 0, 0, false);
part_type_blend(particle_type_player_bit, false);
part_type_color1(particle_type_player_bit, c_black);
	