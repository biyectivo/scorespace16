/// @description 
if (!Game.paused) {
	if (Game.debug && keyboard_check(vk_space)) {
		physics_draw_debug();
	}
	else {
		draw_self();	
	}
}