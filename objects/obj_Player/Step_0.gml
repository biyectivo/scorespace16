/// @description 
if (!Game.paused) {
	if (device_mouse_check_dragswipe_end(0)) {
		var _spd = device_mouse_dragswipe_speed(0) / DRAG_SPEED_DAMPENING;
		var _dir = device_mouse_dragswipe_direction(0);
	
		physics_apply_force(x, y, lengthdir_x(_spd, _dir), lengthdir_y(_spd, _dir));	
	}
	t++;

	//show_debug_message(string(phy_speed)+" "+string(phy_angular_velocity)+" for "+string(num_frames_nearzero));
	if (t > 60 && phy_speed < 0.2 && abs(phy_angular_velocity) < 0.05) {
		num_frames_nearzero++
		if (num_frames_nearzero >= 10) {
			part_emitter_region(Game.particle_system, Game.particle_emitter, x-sprite_width/2, x+sprite_width/2, y-sprite_height/2, y+sprite_height/2, ps_shape_ellipse, ps_distr_gaussian);
			part_emitter_burst(Game.particle_system, Game.particle_emitter, particle_type_player_bit, 200);		
			instance_destroy();
		}
	}
	else {
		num_frames_nearzero = 0;	
	}
}