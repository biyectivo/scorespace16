event_inherited();
var fixture = physics_fixture_create();
physics_fixture_set_polygon_shape(fixture);
physics_fixture_add_point(fixture, 16, 0);
physics_fixture_add_point(fixture, 31, 31);
physics_fixture_add_point(fixture, 0, 31);
//physics_fixture_add_point(fixture, 15, 0);
physics_fixture_set_density(fixture, 0);
physics_fixture_set_restitution(fixture, 0);
physics_fixture_set_linear_damping(fixture, 0);
physics_fixture_set_angular_damping(fixture, 0);
physics_fixture_set_friction(fixture, 0.1);
if (!BROWSER) {
	physics_fixture_bind_ext(fixture, id, -sprite_width/2, -sprite_height/2);
}
else {
	physics_fixture_bind_ext(fixture, id, sprite_width/2, sprite_height/2);
}
physics_fixture_delete(fixture);


phy_rotation = base_rotation;