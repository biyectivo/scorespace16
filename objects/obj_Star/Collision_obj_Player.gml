if (!Game.paused) {
	Game.total_score = Game.total_score + POINTS_STAR;
	if (Game.option_value[? "Sounds"]) {
		audio_play_sound(snd_Star, 2, false);
	}
	var _id = instance_create_layer(x, y, "lyr_Points", obj_Text);
	with (_id) {
		text = "[fa_center][fa_middle][fnt_Title_2][scale,0.2][c_yellow]+"+string(POINTS_STAR);
	}
	var _id = instance_place(x, y, obj_StarSpawner);
	_id.alarm[0] = irandom_range(_id.min_spawner_time, _id.max_spawner_time) * 60;
	instance_destroy();
}