if (!place_meeting(x, y, obj_Star) && !place_meeting(x, y, obj_Superstar)) {
	if (random(1) < 0.7) {
		instance_create_layer(x, y, "lyr_Instances", obj_Star);
	}
	else {
		instance_create_layer(x, y, "lyr_Instances", obj_Superstar);
	}
}
alarm[0] = irandom_range(min_spawner_time, max_spawner_time) * 60;