if (!surface_exists(mask_surface)) {
	mask_surface = surface_create(window_get_width(), window_get_height());	
}

surface_set_target(mask_surface);
gpu_set_blendmode(bm_subtract);
draw_set_color(c_black);
draw_circle(ROOM_MOUSE_X, ROOM_MOUSE_Y, 80, false);
gpu_set_blendmode(bm_normal);
surface_reset_target();

draw_surface(mask_surface, 0, 0);
